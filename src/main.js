import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Vuetify from 'vuetify'
import Vuelidate from 'vuelidate'
import { ApolloClient, createNetworkInterface } from 'apollo-client'
import VueApollo from 'vue-apollo'

// Create the apollo client
const apolloClient = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'http://vabotuandrei.dev/graphql',
    // transportBatching: true,
  }),
  connectToDevTools: true,
  opts: {
    mode: 'no-cors',
  },
})

// Install the vue plugin
Vue.use(VueApollo)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

import {routes} from './routes'

require('./stylus/main.styl')

Vue.use(Vuetify)
Vue.use(Vuelidate)
Vue.use(VueRouter)

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehviour(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }
        if (to.hash) {
            return {
                selector: to.hash
            };
        }
    }
})

new Vue({
  el: '#app',
  router,
  apolloProvider,
  render: h => h(App)
})
