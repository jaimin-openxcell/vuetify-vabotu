export const IndeterminateProgress = {
    data() {
        return {
            indeterminateLinear: false
        }
    },
    methods: {
        showProgressLinear() {
            this.indeterminateLinear = true;
        },
        restProgressLinear() {
            this.indeterminateLinear = false;  
        },
    }
};
