import Home from './components/Home.vue';
import Signin from './components/Signin.vue';
import CreateCompany from './components/CreateCompany.vue';

export const routes = [
    { path:'/', components: {
        default: Home,
    }, name:'home' },

    { path:'/signin', components: {
        default: Signin,
    }, name:'signin' },

    { path:'/create', components: {
        default: CreateCompany,
    }, name:'CreateCompany' },

    { path: '*', redirect: {name:'home'} }
];